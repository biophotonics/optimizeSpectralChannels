%% Readme
%
% This script uses 2 different spectra as an input and calculates spectral 
% bands to optimally separate between the two spectra. 
% CSV files can be imported, default formatting matches Spectragryph format (Spekwin32% ASCII)

addpath('helperfuncs/')
%% Settings
s.loadSpectra       = 1;                                % 1 = Load from CSV, 2 = Load example CSV, 0 = Skip/keep workspace variable 
s.CSVDataFormat     = [ 3, "\t", ",", "." , 1, 1, 3];   % Format of the CSV file: [number of first data row (after header), delimiter, decimal separator, thousands separator, row number for Signal names (0=skip), x-axis column, column no first signal] Default is [ 2, ";", ",", ".", 1, 1, 3];

% Define 5 filter setups / combinations of spectral bands to highlight with markers
s.filter{1}         = [410,490; 510,650];           % + Huang et. al.: [410,490; 510,650]
s.filter{2}         = [408,448 ; 530,647];          % o, purple
s.filter{3}         = [425-25,425+25 ; 495,640];    % 0, green (small) 
s.filter{4}         = [425-25,425+25 ; 510,640];    % square?, green (small) 
s.LP                = [490, 10];                                    % Example: define a dichroic mirror [edge,gap]
s.filter{5}         = [410,s.LP(1)-s.LP(2) ; s.LP(1)+s.LP(2),640];  % diamond?, green (small) 

setup.range             = [425 600];            % range in which the filters can be defined in nm, MAX for QEPro with H1 grating: [374 762], default : [425 650]
setup.bins              = 10;                   % create bins with n number of spectral positions to save calculation time; n=28 matches physical resolution (QEPro, H1 grating, 600�m slit)

%% Features
s.optimizeF         = 1;                    % 0: disable, 1: enable optimizing filter setup 1 for separation (same signal collection)
s.normalize         = 0;                    % 0: No / 1: PMF: sum = 1 / 2: peak within range
s.plotInput         = 1;                    % Plot input signals before calculating
s.channelPlot       = 1;                    % Plot input channels with Filters: F1, F1 optimized, F1 increased separation

s.AllowOverlap      = 0;                    % 0: Simultaneous detection, channels can not overlap, 1(untested): Sequential acquisition, acquisition channels may overlap
setup.correlate     = 0;                    % plot the ratio I1/I2 for each wavelength
s.recalc            = 1;                    % recalculation is necessary after changing input signals, not inbetween changing formulas

%% Design
p.color0  = [211 211 211]/235;   % gray
p.color1  = [  7  89  66]/255;   % green
p.color1b = [  8 166 121]/255;   % green bright
p.color2  = [191 140   0]/255;   % ochre dark
p.color2b = [244 199  73]/255;   % ochre bright

p.color3c = [159 27 179]/255;   % red purple
p.color3b = [166 43 8]/255;     % red complements 1b
p.color3a = [89 20 0]/255;      % red
p.color4b = [33 168 217]/255;   % blu complements 1b
p.color4a = [29 55 64]/255;     % blu complements 1a
 
%% Load CSV or select test signal
% Load new spectra
if s.loadSpectra == 1
    [CSVlegend, CSVdata] = CSVimport(s.CSVDataFormat); % 3 or more row csv file (1 Wavelength & 2 Data rows)
    
% Load spectra from example
elseif s.loadSpectra  == 2
    t.exampleCSV = "data/2D_example.csv";
    t.exampleCSV = "data/2D_shifted.csv"; % shift due to transmission through 20um tissue
    %t.exampleCSV = "data/Spheroid_example.csv";
    [CSVlegend, CSVdata] = CSVimport(s.CSVDataFormat,t.exampleCSV); 
end
%% Calc ALL possible ratios/substractions
if s.recalc == 1
clear('ChannelRatios', 'ChannelSum', 't');
t.nm_in             = CSVdata(:,1);     % wavelength vector (f.ex. from CSVdata(:,1))
t.data1_in          = CSVdata(:,2);     % data vector (f.ex. from CSVdata(:,2))
t.data2_in          = CSVdata(:,3);     % data vector (f.ex. from CSVdata(:,2))
t.pxRange = spec_pos(setup.range,squeeze(t.nm_in));

if s.normalize == 1
t.data1_in          = CSVdata(:,2)/sum(CSVdata(t.pxRange(1):t.pxRange(2),2));     % data vector (f.ex. from CSVdata(:,2))
t.data2_in          = CSVdata(:,3)/sum(CSVdata(t.pxRange(1):t.pxRange(2),3));     % data vector (f.ex. from CSVdata(:,2))
sum(t.data1_in(t.pxRange(1):t.pxRange(2)))
elseif s.normalize == 2
t.data1_in          = CSVdata(t.pxRange(1):t.pxRange(2),2)/max(CSVdata(t.pxRange(1):t.pxRange(2),2));     % data vector (f.ex. from CSVdata(:,2))
t.data2_in          = CSVdata(t.pxRange(1):t.pxRange(2),3)/max(CSVdata(t.pxRange(1):t.pxRange(2),2));     % data vector (f.ex. from CSVdata(:,2))
end

t.waitbar = waitbar(.5,"processing takes 30 sec @10 bins, 10 min? at 5 bins");
t.CHratios1_raw = Calc_PossibleRatios(setup,t.nm_in,t.data1_in);  % ChannelRatios: startNM1, stopNM1, SumCH1, startNM2, stopNM2, SumCH2, CH1./CH2
t.CHratios2_raw = Calc_PossibleRatios(setup,t.nm_in,t.data2_in);
close(t.waitbar);
end
%% Calculate Optimal Channels
if s.AllowOverlap  == 0  % keep all channel combinations, where start CH2 begins after (or at) CH1 
   ChannelRatios1=t.CHratios1_raw(find(t.CHratios1_raw(:,4)>(t.CHratios1_raw(:,2))),:);
   ChannelRatios2=t.CHratios2_raw(find(t.CHratios2_raw(:,4)>(t.CHratios2_raw(:,2))),:);
else
    ChannelRatios1=t.CHratios1_raw;
    ChannelRatios2=t.CHratios2_raw;
end

        Separation     = ( (ChannelRatios1(:,3) ./ ChannelRatios1(:,6))  ./ (ChannelRatios2(:,3) ./ ChannelRatios2(:,6)) ); % (FHC.CH1/FHC.CH2)/(HT29.CH1/HT29CH2)
        for n=1:length(Separation)
        if Separation(n)<1;  Separation(n) = 1/Separation(n); end
        end  
        % every Sensitivity < 1 is Sensitivity = 1/Sensitivity   
    Sensitivity    = sqrt(sqrt(ChannelRatios2(:,3).*ChannelRatios2(:,6)) .* sqrt(ChannelRatios1(:,3).*ChannelRatios1(:,6))); 

%% Plot input spectra/signals

if s.plotInput == 1 % calculate bins
    t.nmBinned = 0; t.data1Binned = 0; t.data2Binned = 0;
for j=1:floor((length(t.nm_in))/(setup.bins))      
    t.nmBinned(j) = mean(t.nm_in((((j-1)*setup.bins)+1):(j*setup.bins)));
    t.data1Binned(j) = sum(t.data1_in((((j-1)*setup.bins)+1):(j*setup.bins)));
    t.data2Binned(j) = sum(t.data2_in((((j-1)*setup.bins)+1):(j*setup.bins)));
end
t.pixelrange = spec_pos(setup.range,t.nmBinned');
if size(t.nmBinned) ~= size(t.data1Binned(j)); disp('wavelength and data vector need to be of same size'); end

global inputPlot
inputPlot = figure;
    plot(t.nmBinned(t.pixelrange(1):t.pixelrange(2)),t.data1Binned(t.pixelrange(1):t.pixelrange(2)),'-o','Color',p.color1); 
    hold on;
    plot(t.nmBinned(t.pixelrange(1):t.pixelrange(2)),t.data2Binned(t.pixelrange(1):t.pixelrange(2)),'-s','Color',p.color2);
    legend(CSVlegend{2:3});hold off;
    title('Binned Signals');
    xlim([setup.range(1)-50,setup.range(2)+50]);
    ylim([min(min(t.data1Binned,t.data1Binned))*0.95,max(max(t.data1Binned,t.data1Binned))*1.05]);
end

if setup.correlate == 1
    Correlation(:,1) = CSVdata(:,1);
    Correlation(:,2) = CSVdata(:,2)./CSVdata(:,3);
    for n=1:length(Correlation)
        if Correlation(n,2)<1
        Correlation(n,3) = 1/Correlation(n,2);    
        else
        Correlation(n,3) = Correlation(n,2);
        end
    end
figure;
    plot(Correlation(t.pxRange(1):t.pxRange(2),1),Correlation(t.pxRange(1):t.pxRange(2),2), '-', 'Color',p.color1); hold on;    
    plot(Correlation(t.pxRange(1):t.pxRange(2),1),Correlation(t.pxRange(1):t.pxRange(2),3), ':','Color',p.color2); hold on;
    legend('S1/S2', '"1/ans if <1"');hold off;
    title('Correlated Signals');
end
%% Plot all Filter combinations and probe filters
output.Vch = figure;
     plot(Sensitivity,Separation,'*','Color',p.color2);
     hold on
   % for n=1:length(fftCalc.Sensitivity); xline(fftCalc.Sensitivity(n),'--r'); end
        t.F1Index = filterFinder(ChannelRatios1,s.filter{1});       % find REDOX filter wavelengths in plot
        plot(Sensitivity(t.F1Index),Separation(t.F1Index),'s','MarkerSize',10,'MarkerEdgeColor','k','MarkerFaceColor','w')% Plot REDOX filters in green
    ylabel('Separation')
    xlabel('Signal (GM)')
    hold off

% Select datapoint in figure and reveal filter combinations
t.N = inputdlg(strcat('How many datapoints would you like to ask for their filter combination?'),'#datapoints',[1 55],{'1'});
if isequal(t.N,{}); return; end

for i = 1:(str2double(cell2mat(t.N)))
c_info = datacursormode(output.Vch);            % define cursor
set(c_info,'Enable','on')                % activate cursor
pause                                    % wait for input
set(c_info,'Enable','off')               % deactivate cursor
t.discard = getCursorInfo(c_info);
t.spots(i,:) = t.discard.Position;
end
close(output.Vch);

% Find selected filter combinations and put them in a table
t.fNo = 0; %avoid old entries, empty array
t.Index = 0;
for n=1:(str2double(cell2mat(t.N)))
    t.Index(n) = wavelengthFinder(Sensitivity,Separation,t,n);  
end
%%
FilterTable = zeros(length([t.Index]),5);
FilterTable(:,2:5) = ChannelRatios1([t.Index],[1 2 4 5]); % Rows from vector, colums lambda 1,2,3,4
FilterTable(:,1)=t.fNo;
%%% Plot Figure with selected spots (chronologically) and Table
figure;
    plot(Sensitivity,Separation,'.','Color',p.color0);
    title([CSVlegend{2}  ' vs '  CSVlegend{3}]);
    hold on
    %yline(1,'--r');
    ylabel('Separation');xlabel('Signal (GM)')
    for n=1:(str2double(cell2mat(t.N))) 
       plot(Sensitivity(t.Index(:,n)),Separation(t.Index(:,n)),'+r','MarkerSize',8)
    end
    
    for k=1:200
        t.tSens_k = k/200*0.5; % half sensitivity half SNR
        if t.tSens_k>0.2
        t.tSensInd_k = find((Sensitivity(:) > t.tSens_k*(1-0.005)) & (Sensitivity(:) < t.tSens_k*(1+0.005))); % list all indices, that start CH1 with redoxWL1
        else
            t.tSensInd_k = find((Sensitivity(:) > t.tSens_k*(1-0.001)) & (Sensitivity(:) < t.tSens_k*(1+0.05))); % list all indices, that start CH1 with redoxWL1
        end
        [t.tSensMaxSep_k, t.tempSensMaxSepIndex_k] = max(Separation(t.tSensInd_k));
        t.tSensMaxSepIndex_k = t.tSensInd_k(t.tempSensMaxSepIndex_k);
        plot(Sensitivity(t.tSensMaxSepIndex_k),Separation(t.tSensMaxSepIndex_k),'.','MarkerSize',10,'MarkerEdgeColor',p.color1b) % Plot filters in green
    end
        t.fStyle = ['o', 'o'];
        t.F1Index = filterFinder(ChannelRatios1,s.filter{1});       % find REDOX filter wavelengths in plot
        plot(Sensitivity(t.F1Index),Separation(t.F1Index),'o','MarkerSize',10,'MarkerEdgeColor',p.color3c,'MarkerFaceColor',p.color3c); %[1 .6 .6]) % Plot REDOX filters
        t.F2Index = filterFinder(ChannelRatios1,s.filter{2});       % find filter wavelengths in plot
        plot(Sensitivity(t.F2Index),Separation(t.F2Index),'o','MarkerSize',12,'MarkerEdgeColor',p.color1b,'MarkerFaceColor','r') % Plot filters in green    
        t.F3Index = filterFinder(ChannelRatios1,s.filter{3});       % find filter wavelengths in plot
        plot(Sensitivity(t.F3Index),Separation(t.F3Index),'^','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','g') % Plot filters in green
        t.F4Index = filterFinder(ChannelRatios1,s.filter{4});       % find filter wavelengths in plot
        plot(Sensitivity(t.F4Index),Separation(t.F4Index),'s','MarkerSize',6,'MarkerEdgeColor',p.color3c,'MarkerFaceColor','g') % Plot filters in green
        t.F5Index = filterFinder(ChannelRatios1,s.filter{5});       % find filter wavelengths in plot
        plot(Sensitivity(t.F5Index),Separation(t.F5Index),'d','MarkerSize',6,'MarkerEdgeColor',p.color3c,'MarkerFaceColor','g') % Plot filters in green
        %legend("1","2","3","4","5");
        
if s.optimizeF == 1
% find closest to F1, max separation
        t.tSens = Sensitivity(t.F1Index);
        t.tSensInd = find((Sensitivity(:) > t.tSens*(1-0.001)) & (Sensitivity(:) < t.tSens*(1+0.001))); % list all indices, that start CH1 with the same WL as F1
        [t.tSensMaxSep, t.tempSensMaxSepIndex] = max(Separation(t.tSensInd));
        t.tSensMaxSepIndex = t.tSensInd(t.tempSensMaxSepIndex);
        plot(Sensitivity(t.tSensMaxSepIndex),Separation(t.tSensMaxSepIndex),'^','MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor','w') % Plot filters
        FilterTable(end+1,2:5) = ChannelRatios1([t.tSensMaxSepIndex],[1 2 4 5]);
        FilterTable(end,6) = 1;

% find closest to sens(redox)/2, max separation
        t.tSens = Sensitivity(t.F1Index)/2;
        t.tSensInd = find((Sensitivity(:) > t.tSens*(1-0.001)) & (Sensitivity(:) < t.tSens*(1+0.001))); % list all indices, that ..
        [t.tSensMaxSep, t.tempSensMaxSepIndex] = max(Separation(t.tSensInd));
        t.tSensMaxSepIndex = t.tSensInd(t.tempSensMaxSepIndex);
        plot(Sensitivity(t.tSensMaxSepIndex),Separation(t.tSensMaxSepIndex),'s','MarkerSize',11,'MarkerEdgeColor',p.color4a,'MarkerFaceColor',p.color4b) % Plot filters
        FilterTable(end+1,2:5) = ChannelRatios1([t.tSensMaxSepIndex],[1 2 4 5]);
        FilterTable(end,6) = 2;
        % find highest for given sens
        
% % find closest to sens=3, max separation
%         t.tSens = 0.3;
%         t.tSensInd = find((Sensitivity(:) > t.tSens*(1-0.001)) & (Sensitivity(:) < t.tSens*(1+0.001))); % list all indices, that ..
%         [t.tSensMaxSep, t.tempSensMaxSepIndex] = max(Separation(t.tSensInd));
%         t.tSensMaxSepIndex = t.tSensInd(t.tempSensMaxSepIndex);
%         plot(Sensitivity(t.tSensMaxSepIndex),Separation(t.tSensMaxSepIndex),'d','MarkerSize',10,'MarkerEdgeColor','w','MarkerFaceColor','k') % Plot filters in green
%         FilterTable(end+1,2:5) = ChannelRatios1([t.tSensMaxSepIndex],[1 2 4 5]);
%         FilterTable(end,6) = 3;
%         % find highest for given sens
end

set(gca, 'Layer', 'Top')
   %{     
% Calc all setups with adjacent filters
    t.totalChannelSum = sum(t.data1Binned((t.pixelrange(1)):(t.pixelrange(end))));% Calculating the actual SUM
    t.adjacentChannels = find(sum(ChannelRatios1(:,[3,6]),2)==t.totalChannelSum);
    plot(Sensitivity(t.adjacentChannels),Separation(t.adjacentChannels),'*r')
    t = rmfield(t,'totalChannelSum');
    t = rmfield(t,'adjacentChannels');
        %}
%% Table
figure('Position',[200 200 420 270]);
title([CSVlegend{2}  ' vs '  CSVlegend{3}]);

output.uit = uitable(gcf,'Data',FilterTable,'Position',[10 10 400 260]);
% t = rmfield(t,'Index'); %avoid plotting old filters
clear temp;

%% Draw Signal w. Channels
p.n = 3; % plot every n markers
p.ch = 2; % = 2 for 2D cultures
if s.channelPlot == 1
figure;
    %plot(t.nmBinned(t.pixelrange(1):t.pixelrange(2)),t.data1Binned(t.pixelrange(1):t.pixelrange(2)),'-','Color',p.color1,'MarkerFaceColor',p.color1b,'HandleVisibility','off'); 
    hold on;
    plot(t.nmBinned(t.pixelrange(1):p.n:t.pixelrange(2)),t.data1Binned(t.pixelrange(1):p.n:t.pixelrange(2)),'-o','Color',p.color1,'MarkerFaceColor',p.color1b);    
    %plot(t.nmBinned(t.pixelrange(1):t.pixelrange(2)),t.data2Binned(t.pixelrange(1):t.pixelrange(2)),'-','Color',p.color2,'MarkerFaceColor',p.color2b);
    plot(t.nmBinned(t.pixelrange(1):p.n:t.pixelrange(2)),t.data2Binned(t.pixelrange(1):p.n:t.pixelrange(2)),'-s','Color',p.color2,'MarkerFaceColor',p.color2b);
    title([CSVlegend{2}  ' vs '  CSVlegend{3}]);
    xlabel('wavelength [nm]');
    ylabel('pmf');
    xlim([setup.range(1)-10,setup.range(2)+10]);
    ylim([min(min(t.data1Binned(t.pixelrange(1):t.pixelrange(2)),t.data2Binned(t.pixelrange(1):t.pixelrange(2))))-0.0025*2,max(max(t.data1Binned(t.pixelrange(1):t.pixelrange(2)),t.data2Binned(t.pixelrange(1):t.pixelrange(2))))*1.05]);
    legend(CSVlegend{2:3});
    
    
%plot fixed sens after huang 
% {
p.d = 0.0006;
p.s=p.d/1.3;
% spheroid py 23 17 11 5

%plot HALF sens (from huang) 
% {
p.y=[0.0, 0.00, 0.001, 0.002]-0.0005*p.ch; % txtpos, min, mean, max %down
p.wl=[FilterTable(end-1,2),FilterTable(end-1,3),FilterTable(end-1,4),FilterTable(end-1,5)]; % insert WL from last query
plot([p.wl(1) p.wl(1)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(2) p.wl(2)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(1) p.wl(2)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(3) p.wl(3)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(4) p.wl(4)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(3) p.wl(4)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(1) p.wl(1)]+8,[p.y(1) p.y(1)]+p.s,'s','MarkerSize',9,'MarkerEdgeColor',p.color4a,'MarkerFaceColor',p.color4b,'HandleVisibility','off'); %[1 .6 .6]) % Plot 
plot([p.wl(3) p.wl(3)]+8,[p.y(1) p.y(1)]+p.s,'s','MarkerSize',9,'MarkerEdgeColor',p.color4a,'MarkerFaceColor',p.color4b,'HandleVisibility','off'); %[1 .6 .6]) % Plot 
text(mean(p.wl(1))+18,p.y(1)++p.d,p.y(1),'high')
text(mean(p.wl(3))+18,p.y(1)++p.d,p.y(1),'separation')
%}

p.y=[0.000, 0.000, 0.001, 0.002]-0.0011*p.ch; % txtpos, min, mean, max %down
p.wl=[FilterTable(end,2),FilterTable(end,3),FilterTable(end,4),FilterTable(end,5)]; % insert WL from last query
plot([p.wl(1) p.wl(1)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(2) p.wl(2)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(1) p.wl(2)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(3) p.wl(3)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(4) p.wl(4)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(3) p.wl(4)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(1) p.wl(1)]+8,[p.y(1) p.y(1)]+p.s,'d','MarkerSize',6,'MarkerEdgeColor','w','MarkerFaceColor','k','HandleVisibility','off'); %[1 .6 .6]) % Plot REDOX filters
plot([p.wl(3) p.wl(3)]+8,[p.y(1) p.y(1)]+p.s,'d','MarkerSize',6,'MarkerEdgeColor','w','MarkerFaceColor','k','HandleVisibility','off'); %[1 .6 .6]) % Plot REDOX filters
text(mean(p.wl(1))+18,p.y(1)+p.d,p.y(1),'fixed')
text(mean(p.wl(3))+18,p.y(1)+p.d,p.y(1),'signal GM = 0.3')
%}

%plot HALF sens (from huang) 
% {
p.y=[0.0, 0.00, 0.001, 0.002]-0.0017*p.ch; % txtpos, min, mean, max %down
p.wl=[FilterTable(end-2,2),FilterTable(end-2,3),FilterTable(end-2,4),FilterTable(end-2,5)]; % insert WL from last query
plot([p.wl(1) p.wl(1)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(2) p.wl(2)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(1) p.wl(2)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(3) p.wl(3)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(4) p.wl(4)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(3) p.wl(4)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(1) p.wl(1)]+8,[p.y(1) p.y(1)]+p.s,'^','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','w','HandleVisibility','off'); %[1 .6 .6]) % Plot REDOX filters
plot([p.wl(3) p.wl(3)]+8,[p.y(1) p.y(1)]+p.s,'^','MarkerSize',6,'MarkerEdgeColor','k','MarkerFaceColor','w','HandleVisibility','off'); %[1 .6 .6]) % Plot REDOX filters
text(mean(p.wl(1))+18,p.y(1)++p.d,p.y(1),'separation')
text(mean(p.wl(3))+18,p.y(1)++p.d,p.y(1),'optimised')
%}

% {
p.y=[0.000, 0.000, 0.001, 0.002]-0.0023*p.ch; % txtpos, min, mean, max
p.wl=[ChannelRatios1(t.F1Index,1:2), ChannelRatios1(t.F1Index,4:5)]; % insert WL from last query
plot([p.wl(1) p.wl(1)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(2) p.wl(2)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(1) p.wl(2)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(3) p.wl(3)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(4) p.wl(4)],[p.y(2) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(3) p.wl(4)],[p.y(3) p.y(3)],'-','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(1) p.wl(1)]+8,[p.y(1) p.y(1)]+0.00045,'o','MarkerSize',7,'MarkerEdgeColor',p.color3c,'MarkerFaceColor',p.color3c,'HandleVisibility','off'); %[1 .6 .6]) % Plot REDOX filters
plot([p.wl(3) p.wl(3)]+8,[p.y(1) p.y(1)]+0.00045,'o','MarkerSize',7,'MarkerEdgeColor',p.color3c,'MarkerFaceColor',p.color3c,'HandleVisibility','off'); %[1 .6 .6]) % Plot REDOX filters
text(mean(p.wl(1))+15,p.y(1)+0.0005,'410-490 nm')
text(mean(p.wl(3))+15,p.y(1)+0.0005,'510-650 nm')
%}
%{
p.y=[0.006, 0.006, 0.007, 0.008]-0.004; % txtpos, min, mean, max
p.wl=[ChannelRatios1(t.FLIndex,1:2), ChannelRatios1(t.FLIndex,4:5)]; % insert WL from last query
plot([p.wl(1) p.wl(1)],[p.y(2) p.y(4)],':','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(2) p.wl(2)],[p.y(2) p.y(4)],':','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(1) p.wl(2)],[p.y(3) p.y(3)],':','Linewidth',1.4,'Color',p.color1,'HandleVisibility','off')
plot([p.wl(3) p.wl(3)],[p.y(2) p.y(4)],':','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(4) p.wl(4)],[p.y(2) p.y(4)],':','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
plot([p.wl(3) p.wl(4)],[p.y(3) p.y(3)],':','Linewidth',1.4,'Color',p.color2,'HandleVisibility','off')
text(mean(p.wl(1:2))-18,p.y(1),'Microscope')
text(mean(p.wl(3:4))-58,p.y(1),'Lyngby')
%}
end

