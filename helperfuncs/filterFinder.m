function index = filterFinder(ChannelRatios1, redoxWL)
 
[~,temp.WL1i]=min(abs(ChannelRatios1(:,1) - redoxWL(1,1)));
temp.WL1 = find(ChannelRatios1(:,1) == ChannelRatios1(temp.WL1i,1)); % list all indices, that start CH1 with redoxWL1
[~,temp.WL2i]=min(abs(ChannelRatios1(:,2) - redoxWL(1,2)));
temp.WL2 = find(ChannelRatios1(:,2) == ChannelRatios1(temp.WL2i,2)); % list all indices, that stopp CH1 with redoxWL2
[~,temp.WL3i]=min(abs(ChannelRatios1(:,4) - redoxWL(2,1)));
temp.WL3 = find(ChannelRatios1(:,4) == ChannelRatios1(temp.WL3i,4)); % list all indices, that start CH2 with redoxWL3
[~,temp.WL4i]=min(abs(ChannelRatios1(:,5) - redoxWL(2,2)));
temp.WL4 = find(ChannelRatios1(:,5) == ChannelRatios1(temp.WL4i,5)); % list all indices, that stop CH2 with redoxWL4

temp.WL12=temp.WL1(find((ismember(temp.WL1, temp.WL2))~=0));    % List all indices, that have the same index for start-CH1 AND stop-CH1
temp.WL13=temp.WL12(find((ismember(temp.WL12, temp.WL3))~=0));  % List all indices, that have the same index for start-CH1 AND stop-CH1 AND start-CH2
temp.WL14=temp.WL13(find((ismember(temp.WL13, temp.WL4))~=0));  % List all indices, that have the same index for start-CH1 AND stop-CH1 AND start-CH2 AND stop-CH2
if length(temp.WL14)~=1;  inputdlg(strcat('multiple indices with the redox WL found. This happens if two wavelengths are rounded up to the same index, typically start2stop1 need to be further apart'),'#warning: works only maybe for one spot',[1 55],{'1'});  
else
index=temp.WL14;
end

end