function Index = wavelengthFinder(Sensitivity,Separation,t,n)   
temp.sensIndices = find(Sensitivity==t.spots(n,1));   % find all occurences of this sensitivity
    if length(temp.sensIndices)==1                       
        Index(n) = temp.sensIndices;                            % checkout if single occurance
    else 
        % now going through all indices comparing separation
        temp.sepIndex = find(Separation==t.spots(n,2));        % all occurances of that sensitivity
        temp.bothIndices = temp.sensIndices(find((ismember(temp.sensIndices, temp.sepIndex))~=0)); % find the one(s) that share the same sensitivity and separation
        if length(temp.bothIndices)==1
            Index(n) = temp.bothIndices;
            t.fNo(n) = n; % selected filter number
        else
            inputdlg(strcat('Multiple indices with the same sensitivity AND separation found: WARNING: TABLE will show 0 for selected spot OR (enable in script) all filter possibilities that lead to this Sensitivity/Separation, ONLY WORKS WITH A SINGLE SELECTED SPOT. (Otherwise Table might be a mess?)'),'#warning: works only maybe for one spot',[1 55],{'1'}); 
            %t.Index(:,n) = 1; %! if multiple filters yield the same point plot first index, this shows wrong filters in FilterTable, but also a colum with skipped Filters is presented
            t.fNo(n:n-1+length(temp.bothIndices)) = n; % selected filter number
            Index(n:n-1+length(temp.bothIndices)) = temp.bothIndices;
        end
    end
    clear temp;
end