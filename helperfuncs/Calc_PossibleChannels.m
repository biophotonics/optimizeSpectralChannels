%% Readme & Setup
%
% This function takes a spectrum and creates all possible virtual channels
% (sums) within a given range
% OUTPUT: LAMBDAstart, LAMBDAstop, Sum of all 

function [ChannelSum, nm] = Calc_PossibleChannels(setup,nm,data)

s.range = setup.range;      % range in which the filters can be defined in nm, MAX [374 762], default: [430 650]
t.RAWnm    = nm;            % wavelength vector (f.ex. from CSVdata(:,1))
t.RAWdata  = data;          % data vector (f.ex. from CSVdata(:,2))
t.bins  = setup.bins;       % bin size for faster results

%% creating bins, discard the last 1...x pixels
for j=1:floor((length(t.RAWnm))/(t.bins))      
    % test(j,1) = ((j-1)*t.bins)+1;  % start index
    % test(j,2) = (j*t.bins);        % stopp index
    t.nm(j) = mean(t.RAWnm((((j-1)*t.bins)+1):(j*t.bins)));
    t.data(j) = sum(t.RAWdata((((j-1)*t.bins)+1):(j*t.bins)));
end
nm = t.nm';
t.pixelrange(1) = spec_pos(s.range(1),t.nm);
t.pixelrange(2) = spec_pos(s.range(2),t.nm);
if size(t.nm) ~= size(t.data); disp('wavelength and data vector need to be of same size'); end

%% Initialise and Calculate execution time
t.rLength  = (t.pixelrange(2)-t.pixelrange(1)+1) ;  % length of the spectral range in indices

t.dLength  = nchoosek(t.rLength,2)+t.rLength;       % "N over K", return all possible combinations / all possible startstop lambdas
ChannelSum = zeros([(t.dLength) 3]);                % M over n or sth

%% Warning for runtime
% t.runtime=num2str(t.dLength*1.6E-5/60,2);
% answer = inputdlg(strcat('Runtime of the script is expected to be ~',t.runtime,' minutes.'),'Runtime',[1 35],{'ok'});
% if isequal(answer,{}); return; end

%% for loop - version
i=1;
for n=1:t.rLength
for m=n:t.rLength
    ChannelSum(i,1) = t.nm(t.pixelrange(1)+n-1); % wavelength CH1 start: lambda_1
    ChannelSum(i,2) = t.nm(t.pixelrange(1)+m-1); % wavelength CH2 stop: lambda_2
    ChannelSum(i,3) = sum(t.data((t.pixelrange(1)+n-1):(t.pixelrange(1)+m-1)));% Calculating the actual SUM
    i=i+1;
end
end
end
