%% Readme & Setup
%
% This function takes a spectrum, then: 
% - first creates all possible virtual channels (sums) within a given range 
% - then creates all possible ratios of two of those channels
%
% A result could be the redox ratio (or an improved version of it)
% The ratios with the best separation for two different spectra is what we
% are looking for ultimatively
%
% Output Format:
% ChannelRatio(?1,?2,CH1_sum,?3,?4,CH2_sum,RATIO) (?1,2 = start/stop CH1 
% (!AVG of BIN, CHANGE??)

function [ChannelRatios, nm] = Calc_PossibleRatios(setup,nm,data)
%% Calc sums
[ChannelSum, nm] = Calc_PossibleChannels(setup,nm,data);
%TESTChannelSum = Calc_PossibleChannels(setup,t.nm_in,t.data1_in);
%ChannelRatios1 = Calc_PossibleRatios(setup,t.nm_in,t.data1_in);

t.cLength  = nchoosek(length(ChannelSum),2);     % "N over K", return all possible combinations of all possible ratios
ChannelRatios = zeros([(t.cLength) 7]);          % M over n or sth

%% Warning for runtime
t.runtime=num2str(t.cLength*7/1.8E6/60,5);  % A test run with 1.8E6 entries ran 7 sec
if ((t.cLength*7/1.8E6/60.5)>10)
answer = inputdlg(strcat('Runtime of the script is expected to be ~',t.runtime,' minutes.'),'Runtime',[1 35],{'ok'});
if isequal(answer,{'ok'}); else; return; end
end
%% Calculate all ratios: ! No self-combination needed !
i=1;
for n=1:length(ChannelSum)
for m=n+1:length(ChannelSum)
    ChannelRatios(i,1:3) = ChannelSum((1+n-1),1:3); % wavelength CH1 start: lambda_1
    ChannelRatios(i,4:6) = ChannelSum((1+m-1),1:3); % wavelength CH2 stop: lambda_2
    %ChannelRatios(i,7) = ChannelSum((1+n-1),3)./ChannelSum((1+m-1),3); % ((t.pixelrange(1)+n-1):(t.pixelrange(1)+m-1)));% Calculating the actual SUM
    ChannelRatios(i,7) = ChannelSum((1+n-1),3)-ChannelSum((1+m-1),3); % ((t.pixelrange(1)+n-1):(t.pixelrange(1)+m-1)));% Calculating 
    i=i+1;
end
end

end
