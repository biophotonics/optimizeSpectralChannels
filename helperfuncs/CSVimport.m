%% Import data from text file
% Script for importing data and legend entries from a given text file:
% Derived from MATLAB auto generator
% INput is a 4 row csv file (1 Wavelength & 2 Data rows, BG row not used as of now)

function [CSVlegend, CSVdata] = CSVimport(CSVDataFormat,fullname)
%% Load file
if ~exist('fullname','var') | fullname == '0'  % Check if filename is given, otherwise open dialogue
    if isfile('private\file.mat')   % load file.mat (contains last used folder)
        load('private\file.mat','file')
    else
        file.path='';
    end
    
    [file.name, file.path] = uigetfile('*.csv;*.dat','select file',file.path);
    
    if file.name~=0
        save('private\file.mat','file') % save to last used folder unless uigetfil was aborted or file loading is scipped
    end    

    file.fullname = fullfile(file.path, file.name);
else
    file.fullname = fullname;
end
%% Import Data: Options

xCol = str2num(CSVDataFormat(6)); % Column containing the x axis
y1Col = str2num(CSVDataFormat(7)); % Colum containing y1
y2Col = str2num(CSVDataFormat(7))+1; % Colum containing y2

opts = delimitedTextImportOptions("NumVariables", 10);

% Specify range and delimiter
opts.DataLines = [str2num(CSVDataFormat(1)), Inf];
opts.Delimiter = CSVDataFormat(2);

% Specify column names and types
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];
opts = setvaropts(opts, "TrimNonNumeric", true);
opts = setvaropts(opts, "DecimalSeparator", CSVDataFormat(3));
opts = setvaropts(opts, "ThousandsSeparator", CSVDataFormat(4));
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Import the data
CSVtable = readtable(file.fullname, opts);
CSVdata = table2array(CSVtable(:,[xCol, y1Col, y2Col]));     % Convert to output type
clear opts                          % Clear temporary variables

%% Import Legend: Options
if str2num(CSVDataFormat(5)) ~= 0
opts = delimitedTextImportOptions("NumVariables", 10);

% Specify range and delimiter
opts.DataLines = [str2num(CSVDataFormat(5)), str2num(CSVDataFormat(5))];
opts.Delimiter = CSVDataFormat(2);

% Specify column names and types
opts.VariableTypes = ["string", "string", "string", "string", "string", "string", "string", "string", "string", "string"];
opts = setvaropts(opts, "WhitespaceRule", "preserve");
opts = setvaropts(opts, "EmptyFieldRule", "auto");
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Import the data
CSVlegend = readtable(file.fullname, opts);
CSVlegend = table2array(CSVlegend(:,[xCol, y1Col, y2Col])); % Convert to output type
CSVlegend(1) = 'Wavelength';
clear opts
else
    CSVlegend{1} = 'Wavelength';
    CSVlegend{2} = 'Spectrum 1';
    CSVlegend{3} = 'Spectrum 2';
end
end