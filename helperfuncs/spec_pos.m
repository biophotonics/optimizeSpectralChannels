%% Readme 
% This Function converts a wavelength to an index of a spectrum
% index = spec_pos(spot,setup,LambdaAxis)

function index = spec_pos(spot,LambdaAxis)
[~,index]= min(abs(LambdaAxis - spot));
end