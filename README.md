Readme

Load .csv/.dat with 2 different spectra. 


CSV format setup:

The format is set using the variable: 
s.CSVDataFormat = [3, "\t" , "," , "." , 1 , 1 , 3]; With the parameters:
- number of first data row (after header)
- delimiter
- decimal separator
- thousands separator
- row number for Signal names (0=skip)
- x-axis column
- column number of first signal (second signal is assumed to be adjacent by CSVimport)

Exporting from Spectragryph to the default CSV format:
- Save as Spekwin32 ASCII [x..yyyy]
- rename to *.CSV (optional)
